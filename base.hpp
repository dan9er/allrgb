/* Part of https://gitlab.com/dan9er/allrgb
 * Copyright (c) 2023 dan9er
 * ISC License, see LICENSE or https://choosealicense.com/licenses/isc/
 *
 * Taken from https://git.suckless.org/farbfeld/file/util.h.html
 * Copyright 2014-2018 Laslo Hunhold <dev@frign.de>
 * Copyright 2004 Ted Unangst <tedu@openbsd.org>
 * Copyright 2004 Todd C. Miller <Todd.Miller@courtesan.com>
 * Copyright 2008 Otto Moerbeek <otto@drijf.net>
 * Copyright 2014-2015 Dimitris Papastamos <sin@2f30.org>
 * Copyright 2014-2016 Hiltjo Posthuma <hiltjo@codemadness.org>
 * Copyright 2015 Willy Goiffon <willy@mailoo.org>
 * Copyright 2016 Alexander Krotov <ilabdsf@yandex.ru>
 * Copyright 2017 Mattias Andrée <maandree@kth.se>
 * ISC License */

#pragma once

#include <cstdarg>
#include <cstdint>
#include <cstdio>

void die(const char *fmt, ...);

void efwrite(const void* p, const std::size_t s, const std::size_t n, FILE* f);
void efputs(const char* s, FILE* f);

void efread(void* p, const size_t s, const size_t n, FILE* f);
