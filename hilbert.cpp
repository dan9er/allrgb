/* Part of https://gitlab.com/dan9er/allrgb
 * Copyright (c) 2023 dan9er
 * ISC License, see LICENSE or https://choosealicense.com/licenses/isc/
 *
 * Based off public domain code at https://github.com/rawrunprotected/hilbert_curves */

#include "hilbert.hpp"

#include <cstdint>
#include <utility>

namespace hilbert {

static
uint32_t
deinterleave(uint32_t x)
{
	x = x & 0x55555555;
	x = (x | (x >> 1)) & 0x33333333;
	x = (x | (x >> 2)) & 0x0F0F0F0F;
	x = (x | (x >> 4)) & 0x00FF00FF;
	x = (x | (x >> 8)) & 0x0000FFFF;
	return x;
}

static
uint32_t
interleave(uint32_t x)
{
	x = (x | (x << 8)) & 0x00FF00FF;
	x = (x | (x << 4)) & 0x0F0F0F0F;
	x = (x | (x << 2)) & 0x33333333;
	x = (x | (x << 1)) & 0x55555555;
	return x;
}

static
uint32_t
prefixScan(uint32_t x)
{
	x = (x >> 8) ^ x;
	x = (x >> 4) ^ x;
	x = (x >> 2) ^ x;
	x = (x >> 1) ^ x;
	return x;
}

uint32_t
xy2d(const uint32_t x, const uint32_t y)
{
	uint32_t A, B, C, D;

	// Initial prefix scan round, prime with x and y
	{
		uint32_t a = x ^ y;
		uint32_t b = 0xFFFF ^ a;
		uint32_t c = 0xFFFF ^ (x | y);
		uint32_t d = x & (y ^ 0xFFFF);

		A = a | (b >> 1);
		B = (a >> 1) ^ a;

		C = ((c >> 1) ^ (b & (d >> 1))) ^ c;
		D = ((a & (c >> 1)) ^ (d >> 1)) ^ d;
	}

	{
		uint32_t a=A, b=B, c=C, d=D;

		A = ((a & (a >> 2)) ^ (b & (b >> 2)));
		B = ((a & (b >> 2)) ^ (b & ((a ^ b) >> 2)));

		C ^= ((a & (c >> 2)) ^ (b & (d >> 2)));
		D ^= ((b & (c >> 2)) ^ ((a ^ b) & (d >> 2)));
	}

	{
		uint32_t a=A, b=B, c=C, d=D;

		A = ((a & (a >> 4)) ^ (b & (b >> 4)));
		B = ((a & (b >> 4)) ^ (b & ((a ^ b) >> 4)));

		C ^= ((a & (c >> 4)) ^ (b & (d >> 4)));
		D ^= ((b & (c >> 4)) ^ ((a ^ b) & (d >> 4)));
	}

	// Final round and projection
	{
		uint32_t a=A, b=B, c=C, d=D;

		C ^= ((a & (c >> 8)) ^ (b & (d >> 8)));
		D ^= ((b & (c >> 8)) ^ ((a ^ b) & (d >> 8)));
	}

	// Undo transformation prefix scan
	uint32_t a = C ^ (C >> 1);
	uint32_t b = D ^ (D >> 1);

	// Recover index bits
	uint32_t i0 = x ^ y;
	uint32_t i1 = b | (0xFFFF ^ (i0 | a));

	return (interleave(i1) << 1) | interleave(i0);
}

std::pair<uint32_t,uint32_t>
d2xy(const uint32_t d)
{
	uint32_t i0 = deinterleave(d);
	uint32_t i1 = deinterleave(d >> 1);

	uint32_t t0 = (i0 | i1) ^ 0xFFFF;
	uint32_t t1 = i0 & i1;

	uint32_t prefixT0 = prefixScan(t0);
	uint32_t prefixT1 = prefixScan(t1);

	uint32_t a = (((i0 ^ 0xFFFF) & prefixT1) | (i0 & prefixT0));

	return {a ^ i1, a ^ i0 ^ i1};
}

}
