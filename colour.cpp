/* Part of https://gitlab.com/dan9er/allrgb
 * Copyright (c) 2023 dan9er
 * ISC License, see LICENSE or https://choosealicense.com/licenses/isc/ */

#include "colour.hpp"

#include <cstdint>

#include <endian.h>

#include "base.hpp"

namespace colour {

//================//
// PUBLIC MEMBERS //
//================//

template<>
f64
u8::as_f64() const
{
	return f64(
		m_data[0]/255.0,
		m_data[1]/255.0,
		m_data[2]/255.0
	);
}

template<>
f64
u16::as_f64() const
{
	return f64(
		m_data[0]/65535.0,
		m_data[1]/65535.0,
		m_data[2]/65535.0
	);
}

template<>
f64
f64::as_f64() const
	{return *this;}

//=============//
// NON-MEMBERS //
//=============//

void
write_pixel(const u8 p)
{
	uint_least64_t rgba16buf;
	memset(&rgba16buf, 0x0000000000000000, sizeof(uint_least64_t));

	rgba16buf = htobe64(
		(	  ((uint64_t)p[0] << 48)
			| ((uint64_t)p[1] << 32)
			| ((uint64_t)p[2] << 16)
			|  (uint64_t)255
		) * 0x101 /* x |= x << 8 */
	);

	efwrite(&rgba16buf, 2, 4, stdout);
}

u16
read_pixel()
{
	uint_least64_t rgba16buf;

	efread(&rgba16buf, 2, 4, stdin);
	rgba16buf = be64toh(rgba16buf);

	return u16(
		rgba16buf >> 48,
		rgba16buf >> 32,
		rgba16buf >> 16
	);
}

}
