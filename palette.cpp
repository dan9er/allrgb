/* Part of https://gitlab.com/dan9er/allrgb
 * Copyright (c) 2023 dan9er
 * ISC License, see LICENSE or https://choosealicense.com/licenses/isc/ */

#include "palette.hpp"

#include <cstdint>
#include <utility>
#include <vector>

#include "colour.hpp"

namespace palette {

//=======//
// CTORS //
//=======//

template <>
u8f64::palette(ref_array_type&& r) :
	m_ref(r)
{
	m_data.reserve(m_ref.size());
	for (const ref_colour_type& i : m_ref) {
		m_data.emplace_back(i.as_f64());
	}
}

//=============//
// NON-MEMBERS //
//=============//

u8f64
make_allrgb()
{
	u8f64::ref_array_type v;
	v.reserve(256*256*256);

	for (uint16_t r = 0; r < 256; ++r) {
		for (uint16_t g = 0; g < 256; ++g) {
			for (uint16_t b = 0; b < 256; ++b) {
				v.emplace_back(
					static_cast<uint8_t>(r),
					static_cast<uint8_t>(g),
					static_cast<uint8_t>(b)
				);
			}
		}
	}

	return u8f64(std::move(v));
}

} // ns palette
