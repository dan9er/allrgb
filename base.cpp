/* Part of https://gitlab.com/dan9er/allrgb
 * Copyright (c) 2023 dan9er
 * ISC License, see LICENSE or https://choosealicense.com/licenses/isc/
 *
 * Taken from https://git.suckless.org/farbfeld/file/util.c.html
 * Copyright 2014-2018 Laslo Hunhold <dev@frign.de>
 * Copyright 2004 Ted Unangst <tedu@openbsd.org>
 * Copyright 2004 Todd C. Miller <Todd.Miller@courtesan.com>
 * Copyright 2008 Otto Moerbeek <otto@drijf.net>
 * Copyright 2014-2015 Dimitris Papastamos <sin@2f30.org>
 * Copyright 2014-2016 Hiltjo Posthuma <hiltjo@codemadness.org>
 * Copyright 2015 Willy Goiffon <willy@mailoo.org>
 * Copyright 2016 Alexander Krotov <ilabdsf@yandex.ru>
 * Copyright 2017 Mattias Andrée <maandree@kth.se>
 * ISC License */

#include "base.hpp"

#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <cstring>

static void
verr(const char *fmt, va_list ap)
{
	vfprintf(stderr, fmt, ap);

	if (fmt[0] && fmt[strlen(fmt) - 1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	} else {
		fputc('\n', stderr);
	}
}

void
die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	verr(fmt, ap);
	va_end(ap);

	exit(1);
}

void
efwrite(const void* p, const std::size_t s, const std::size_t n, FILE* f)
{
	if (fwrite(p, s, n, f) != n)
		die("fwrite:");
}

void
efputs(const char* s, FILE* f)
{
	if (fputs(s, f) == EOF)
		die("fputs:");
}

void
efread(void* p, const size_t s, const size_t n, FILE* f)
{
	if (fread(p, s, n, f) != n) {
		if (ferror(f))
			die("fread:");
		else
			die("fread: Unexpected end of file");
	}
}
