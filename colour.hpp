/* Part of https://gitlab.com/dan9er/allrgb
 * Copyright (c) 2023 dan9er
 * ISC License, see LICENSE or https://choosealicense.com/licenses/isc/
 *
 * srgb2lrgb_s & lrgb2srgb_s based off https://git.suckless.org/blind/file/src/util/colour.h.html
 * © 2017 Mattias Andrée <maandree@kth.se>
 * ISC License */

#pragma once

#include <array>
#include <cmath>
#include <cstdint>
#include <type_traits>
#include <utility>
#include <eigen3/Eigen/Core>

namespace colour {

template<typename T>
class colour3;

//=========//
// ALIASES //
//=========//

using u8  = colour3<uint8_t>;
using u16 = colour3<uint16_t>;
using f64 = colour3<double>;

template<typename T>
class colour3
{
public:
	//===========//
	// TYPENAMES //
	//===========//

	using vector_type = Eigen::Matrix<T,3,1>;

	//=======//
	// CTORS //
	//=======//

	colour3() = default;
	colour3(const std::array<T,3>&);
	colour3(std::array<T,3>&&);
	colour3(const T, const T, const T);
	colour3(const vector_type&);
	colour3(vector_type&&);

	//================//
	// PUBLIC MEMBERS //
	//================//

	T& operator[](std::size_t);
	const T& operator[](std::size_t) const;

	f64 as_f64() const;

	colour3& srgb2lrgb();
	colour3& lrgb2srgb();

	Eigen::Map<vector_type> as_vector();

	constexpr uint32_t flat_index() const;

private:
	//======//
	// DATA //
	//======//

	std::array<T,3> m_data;
};

//=============//
// NON-MEMBERS //
//=============//

template<typename F>
F srgb2lrgb_s(const F);

template<typename F>
F lrgb2srgb_s(const F);

void write_pixel(const u8);
u16 read_pixel();

//===================================//

//=======//
// CTORS //
//=======//

template<typename T>
colour3<T>::colour3(const std::array<T,3>& d) :
	m_data(d)
{}

template<typename T>
colour3<T>::colour3(std::array<T,3>&& d) :
	m_data(std::move(d))
{}

template<typename T>
colour3<T>::colour3(const T a, const T b, const T c) :
	m_data({a,b,c})
{}

template<typename T>
colour3<T>::colour3(const vector_type& v) :
	m_data({v[0],v[1],v[2]})
{}

template<typename T>
colour3<T>::colour3(vector_type&& v) :
	m_data({std::move(v[0]),std::move(v[1]),std::move(v[2])})
{}

//================//
// PUBLIC MEMBERS //
//================//

template<typename T>
T&
colour3<T>::operator[](std::size_t n)
	{return m_data[n];}
template<typename T>
const T&
colour3<T>::operator[](std::size_t n) const
	{return m_data[n];}

template<typename F>
colour3<F>&
colour3<F>::srgb2lrgb()
{
	static_assert(std::is_floating_point<F>::value, "type must be floating point");
	m_data[0] = srgb2lrgb_s(m_data[0]);
	m_data[1] = srgb2lrgb_s(m_data[1]);
	m_data[2] = srgb2lrgb_s(m_data[2]);
	return *this;
}

template<typename F>
colour3<F>&
colour3<F>::lrgb2srgb()
{
	static_assert(std::is_floating_point<F>::value, "type must be floating point");
	m_data[0] = lrgb2srgb_s(m_data[0]);
	m_data[1] = lrgb2srgb_s(m_data[1]);
	m_data[2] = lrgb2srgb_s(m_data[2]);
	return *this;
}

template<typename F>
Eigen::Map<typename colour3<F>::vector_type>
colour3<F>::as_vector()
	{return Eigen::Map<vector_type>(m_data.data());}

template<>
constexpr
uint32_t
colour3<uint8_t>::flat_index() const
	{return (65536*m_data[0]) + (256*m_data[1]) + m_data[2];}

//=============//
// NON-MEMBERS //
//=============//

template<typename F>
F
srgb2lrgb_s(const F x)
{
	static_assert(std::is_floating_point<F>::value, "type must be floating point");
	return x <= (F)0.040448236277380503536l ?
		x / (F)12.92l
	:
		std::pow((x + (F)0.055l) / (F)1.055l, (F)2.4l);
}

template<typename F>
F
lrgb2srgb_s(const F x)
{
	static_assert(std::is_floating_point<F>::value, "type must be floating point");
	return x <= (F)0.0031306684425217108l ?
		x * (F)12.92l
	:
		(std::pow(x, ((F)5.0l/12)) * (F)1.055l) - (F)0.055l;
}

} // ns colour
