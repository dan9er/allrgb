/* Part of https://gitlab.com/dan9er/allrgb
 * Copyright (c) 2023 dan9er
 * ISC License, see LICENSE or https://choosealicense.com/licenses/isc/
 *
 * M1/2 values taken from https://bottosson.github.io/posts/oklab/#converting-from-linear-srgb-to-oklab
 * Copyright (c) 2020 Björn Ottosson
 * Public domain / MIT License
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. */

#include "oklab.hpp"

#include <eigen3/Eigen/Dense>

/* from Oklab blogpost */
namespace oklab {

const Eigen::Matrix3d M1 = (Eigen::Matrix3d() <<
	0.4122214708, 0.5363325363, 0.0514459929,
	0.2119034982, 0.6806995451, 0.1073969566,
	0.0883024619, 0.2817188376, 0.6299787005
).finished();

const Eigen::Matrix3d M2 = (Eigen::Matrix3d() <<
	0.2104542553, 0.7936177850,-0.0040720468,
	1.9779984951,-2.4285922050, 0.4505937099,
	0.0259040371, 0.7827717662,-0.8086757660
).finished();

}
