/* Part of https://gitlab.com/dan9er/allrgb
 * Copyright (c) 2023 dan9er
 * ISC License, see LICENSE or https://choosealicense.com/licenses/isc/
 *
 * read/write_header based off https://git.suckless.org/farbfeld/file/util.c.html
 * Copyright 2014-2018 Laslo Hunhold <dev@frign.de>
 * Copyright 2004 Ted Unangst <tedu@openbsd.org>
 * Copyright 2004 Todd C. Miller <Todd.Miller@courtesan.com>
 * Copyright 2008 Otto Moerbeek <otto@drijf.net>
 * Copyright 2014-2015 Dimitris Papastamos <sin@2f30.org>
 * Copyright 2014-2016 Hiltjo Posthuma <hiltjo@codemadness.org>
 * Copyright 2015 Willy Goiffon <willy@mailoo.org>
 * Copyright 2016 Alexander Krotov <ilabdsf@yandex.ru>
 * Copyright 2017 Mattias Andrée <maandree@kth.se>
 * ISC License */

#include "img.hpp"

#include <cstdint>
#include <cstdio>
#include <cstring>

#include <endian.h>

#include "base.hpp"
#include "colour.hpp"

#define LEN(x) (sizeof (x) / sizeof *(x))

namespace img {

//=============//
// NON-MEMBERS //
//=============//

void
write_header(const uint32_t width, const uint32_t height)
{
	uint32_t tmp;

	efputs("farbfeld", stdout);

	tmp = htobe32(width);
	efwrite(&tmp, sizeof(tmp), 1, stdout);

	tmp = htobe32(height);
	efwrite(&tmp, sizeof(tmp), 1, stdout);
}

std::pair<uint32_t,uint32_t>
read_header()
{
	uint32_t header[4];

	efread(header, sizeof(*header), LEN(header), stdin);

	if (memcmp("farbfeld", header, sizeof("farbfeld") - 1))
		die("Invalid magic value");

	return {be32toh(header[2]),be32toh(header[3])};
}

}
