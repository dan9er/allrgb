# allRGB Generators

Repository of the generator programs I used to make my https://allrgb.com entries.

## Index

| Submission | File/Binary |
| --- | --- |
| [Distance From OkZero](https://allrgb.com/distance-from-okzero) | [oklab-distance](./oklab-distance.cpp) |
| N/A (too similar to above) | [oklab-lightness](./oklab-lightness.cpp) |
| [OkChroma](https://allrgb.com/okchroma) | [oklab-chroma](./oklab-chroma.cpp) |
| [Plan OkA](https://allrgb.com/plan-oka) | modified [oklab-lightness](./oklab-lightness.cpp) |
| [Plan OkB](https://allrgb.com/plan-okb) | modified [oklab-lightness](./oklab-lightness.cpp) |
| [OkHue](https://allrgb.com/okhue) | [oklab-hue](./oklab-hue.cpp) |
| TBA | [oklab-quant](./oklab-quant.cpp) |

## Building

Most generators depend on [Eigen](https://eigen.tuxfamily.org/index.php) 3.3+ (some 3.4+).

```sh
git clone https://gitlab.com/dan9er/allrgb.git # --depth=1, etc. to taste
cd allrgb
${editor of choice} config.mk # edit build config for your system
make # ${name of specific binary above}, -j#, -k, etc. to taste
```

## Legal

New code licensed under the ISC Licence, see [LICENSE](./LICENSE).

Licences for third-party code are included inside the files they're located.
