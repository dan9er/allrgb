# Part of https://gitlab.com/dan9er/allrgb
# Copyright (c) 2023 dan9er
# ISC License, see LICENSE or https://choosealicense.com/licenses/isc/

.POSIX:

include config.mk

BIN = oklab-distance oklab-lightness oklab-chroma oklab-hue oklab-quant
oklab-distance.o_REQ  = base colour img hilbert oklab
oklab-lightness.o_REQ = base colour img hilbert oklab
oklab-chroma.o_REQ    = base colour img hilbert oklab
oklab-hue.o_REQ       = base colour img hilbert oklab
oklab-quant.o_REQ     = base colour img oklab palette

all: $(BIN)

oklab-distance: oklab-distance.o $(oklab-distance.o_REQ:=.o)
oklab-distance.o: oklab-distance.cpp config.mk $($@_REQ:=.hpp)

oklab-lightness: oklab-lightness.o $(oklab-lightness.o_REQ:=.o)
oklab-lightness.o: oklab-lightness.cpp config.mk $($@_REQ:=.hpp)

oklab-chroma: oklab-chroma.o $(oklab-chroma.o_REQ:=.o)
oklab-chroma.o: oklab-chroma.cpp config.mk $($@_REQ:=.hpp)

oklab-hue: oklab-hue.o $(oklab-hue.o_REQ:=.o)
oklab-hue.o: oklab-hue.cpp config.mk $($@_REQ:=.hpp)

oklab-quant: oklab-quant.o $(oklab-quant.o_REQ:=.o)
oklab-quant.o: oklab-quant.cpp config.mk $($@_REQ:=.hpp)

.o:
	$(CXX) -o $@ $(LDFLAGS) $< $($@.o_REQ:=.o)

.cpp.o:
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $<

clean:
	rm -f $(BIN) $(BIN:=.o) $(oklab-distance.o_REQ:=.o) $(oklab-lightness.o_REQ:=.o) $(oklab-chroma.o_REQ:=.o) $(oklab-quant.o_REQ:=.o)
