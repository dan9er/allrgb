/* Part of https://gitlab.com/dan9er/allrgb
 * Copyright (c) 2023 dan9er
 * ISC License, see LICENSE or https://choosealicense.com/licenses/isc/ */

#include <cmath>
#include <cstdint>
#include <iostream>
#include <map>
#include <memory>
#include <utility>
#include <eigen3/Eigen/Dense>

#include "colour.hpp"
#include "img.hpp"
#include "hilbert.hpp"
#include "oklab.hpp"

int
main()
{
	// to sort colors
	std::map<std::pair<long double,double>, colour::u8> seq;

	{
		// generate input matrix of lRGB values
		std::cerr << "generating X" << std::endl;
		Eigen::Matrix3Xd X;
		X.resize(Eigen::NoChange,4096*4096);
		for (uint16_t r = 0; r < 256; ++r) {
			for (uint16_t g = 0; g < 256; ++g) {
				for (uint16_t b = 0; b < 256; ++b) {
					const colour::u8 i(r,g,b);
					X.col(i.flat_index()) = i.as_f64().srgb2lrgb().as_vector();
				}
			}
		}

		// X := M2*(M1*X)^o1/3
		std::cerr << "converting to Oklab" << std::endl;
		X = oklab::M2*((oklab::M1*X).array().pow(1.0/3.0).matrix());

		// move hue & lightness values into map
		// TODO 23 values collide in hue, using lightness for tiebreak
		std::cerr << "computing hues" << std::endl;
		auto i = X.colwise().cbegin();
		for (uint16_t r = 0; r < 256; ++r) {
			for (uint16_t g = 0; g < 256; ++g) {
				for (uint16_t b = 0; b < 256; ++b) {
					seq.insert({{atan2l((*i)[2],(*i)[1]),(*i)[0]},colour::u8(r,g,b)});
					++i;
				}
			}
		}
	} // free X, which we don't need anymore

	// create Hilbert curve
	std::cerr << "generating Hilbert curve" << std::endl;
	img::u8<4096,4096> img;
	uint32_t d = 0;
	for (const auto& i : seq) {
		const auto j = hilbert::d2xy(d++);
		img(j.first,4095-j.second) = std::move(i.second);
	}

	// output farbfeld image
	std::cerr << "printing ff image" << std::endl;
	img::write_img(img);

	return 0;
}
