/* Part of https://gitlab.com/dan9er/allrgb
 * Copyright (c) 2023 dan9er
 * ISC License, see LICENSE or https://choosealicense.com/licenses/isc/
 *
 * Based off public domain code at https://github.com/rawrunprotected/hilbert_curves */

#pragma once

#include <cstdint>
#include <utility>

namespace hilbert {

/* hardcoded n=16. technically only n=12 is needed, but n=16 gives the same
 * output and gets rid of some shifts */
std::pair<uint32_t,uint32_t> d2xy(const uint32_t d);
uint32_t xy2d(const uint32_t x, const uint32_t y);

}
