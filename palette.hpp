/* Part of https://gitlab.com/dan9er/allrgb
 * Copyright (c) 2023 dan9er
 * ISC License, see LICENSE or https://choosealicense.com/licenses/isc/ */

#pragma once

#include <cmath>
#include <cstdint>
#include <iterator>
#include <utility>
#include <vector>
#include <eigen3/Eigen/Core>

#include "colour.hpp"
#include "oklab.hpp"

namespace palette {

template <typename R, typename F>
class palette
{
public:
	//===========//
	// TYPENAMES //
	//===========//

	using ref_colour_type = colour::colour3<R>;
	using ref_array_type = std::vector<ref_colour_type>;
	using ref_iterator = typename ref_array_type::iterator;
	using ref_const_iterator = typename ref_array_type::const_iterator;

	using data_colour_type = colour::colour3<F>;
	using data_array_type = std::vector<data_colour_type>;
	using data_iterator = typename data_array_type::iterator;
	using data_const_iterator = typename data_array_type::const_iterator;

	// .second is quant error
	using result_type = std::pair<ref_colour_type,data_colour_type>;

	using matrix_type = Eigen::Matrix<F,3,Eigen::Dynamic>;

	//=======//
	// CTORS //
	//=======//

	palette(ref_array_type&&);

	//================//
	// PUBLIC MEMBERS //
	//================//

	result_type find_pop(data_colour_type);

	Eigen::Map<matrix_type> as_matrix();

	palette& srgb2lrgb();
	palette& lrgb2oklab();

private:
	//======//
	// DATA //
	//======//

	ref_array_type m_ref;
	data_array_type m_data;
};

//=========//
// ALIASES //
//=========//

using u8f64 = palette<uint8_t,double>;

//=============//
// NON-MEMBERS //
//=============//

u8f64 make_allrgb();

//===================================//

//================//
// PUBLIC MEMBERS //
//================//

template <typename R, typename F>
typename palette<R,F>::result_type
palette<R,F>::find_pop(typename palette<R,F>::data_colour_type y)
{
	// UB if *this is empty

	// compute differences
	typename palette<R,F>::matrix_type X = -as_matrix();
	X.colwise() += y.as_vector();

	// store column b/e iters
	const auto begin = X.colwise().begin();
	const auto end = X.colwise().end();

	// find shortest column
	auto min_iter = end;
	F min_sqdist = INFINITY;
	for (auto i = begin; i < end; ++i) {
		F sqdist = i->squaredNorm();
		if (sqdist < min_sqdist) {
			min_sqdist = sqdist;
			min_iter = i;
		}
	}

	// save values
	auto min_n = std::distance(begin,min_iter);
	auto ret = result_type(std::move(m_ref[min_n]),std::move(*min_iter));

	// pop colour from both arrays
	m_ref.erase(m_ref.cbegin() + min_n);
	m_data.erase(m_data.cbegin() + min_n);

	// done
	return ret;
}

template <typename R, typename F>
Eigen::Map<typename palette<R,F>::matrix_type>
palette<R,F>::as_matrix()
	{return Eigen::Map<matrix_type>(&(m_data[0][0]), 3,m_data.size());}

template <typename R, typename F>
palette<R,F>&
palette<R,F>::srgb2lrgb()
{
	for (data_colour_type& i : m_data)
		i.srgb2lrgb();
	return *this;
}

template <typename R, typename F>
palette<R,F>&
palette<R,F>::lrgb2oklab()
{
	matrix_type X = as_matrix();
	X = oklab::M2*((oklab::M1*X).array().pow(1.0/3.0).matrix());
	return *this;
}

} // ns palette
