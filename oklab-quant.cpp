/* Part of https://gitlab.com/dan9er/allrgb
 * Copyright (c) 2023 dan9er
 * ISC License, see LICENSE or https://choosealicense.com/licenses/isc/ */

#include <iostream>
#include <utility>

#include "colour.hpp"
#include "img.hpp"
#include "palette.hpp"

static constexpr uint32_t N = 4096, M = 4096;

int
main()
{
	// make palette
	std::cerr << "generating palette" << std::endl;
	palette::u8f64 pal(palette::make_allrgb().srgb2lrgb().lrgb2oklab());

	// read image
	std::cerr << "reading image" << std::endl;
	img::f64<N,M> img(std::move(img::read_img<N,M>().srgb2lrgb().lrgb2oklab()));

	// quantize (no dithering)
	std::cerr << "quantizing" << std::endl;
	img::write_header(N,M);
	for (const img::f64<N,M>::colour_type& i : img) {
		colour::write_pixel(pal.find_pop(i).first);
	}

	return 0;
}
