/* Part of https://gitlab.com/dan9er/allrgb
 * Copyright (c) 2023 dan9er
 * ISC License, see LICENSE or https://choosealicense.com/licenses/isc/
 *
 * read/write_header based off https://git.suckless.org/farbfeld/file/util.h.html
 * Copyright 2014-2018 Laslo Hunhold <dev@frign.de>
 * Copyright 2004 Ted Unangst <tedu@openbsd.org>
 * Copyright 2004 Todd C. Miller <Todd.Miller@courtesan.com>
 * Copyright 2008 Otto Moerbeek <otto@drijf.net>
 * Copyright 2014-2015 Dimitris Papastamos <sin@2f30.org>
 * Copyright 2014-2016 Hiltjo Posthuma <hiltjo@codemadness.org>
 * Copyright 2015 Willy Goiffon <willy@mailoo.org>
 * Copyright 2016 Alexander Krotov <ilabdsf@yandex.ru>
 * Copyright 2017 Mattias Andrée <maandree@kth.se>
 * ISC License */

#pragma once

#include <array>
#include <cstdint>
#include <memory>
#include <eigen3/Eigen/Core>

#include "base.hpp"
#include "colour.hpp"
#include "oklab.hpp"

namespace img {

template <typename T, uint32_t N, uint32_t M>
class img
{
public:
	//===========//
	// TYPENAMES //
	//===========//

	using colour_type = colour::colour3<T>;
	using array_type = std::array<colour_type,N*M>;
	using iterator = typename array_type::iterator;
	using const_iterator = typename array_type::const_iterator;

	// dynamic because too big for stack, Eigen would balk at compile-time
	using matrix_type = Eigen::Matrix<T,3,Eigen::Dynamic>;

	//=======//
	// CTORS //
	//=======//

	img();

	//================//
	// PUBLIC MEMBERS //
	//================//

	colour_type& operator()(const uint32_t x, const uint32_t y);
	const colour_type& operator()(const uint32_t x, const uint32_t y) const;

	iterator begin() noexcept;
	const_iterator begin() const noexcept;
	const_iterator cbegin() const noexcept;

	iterator end() noexcept;
	const_iterator end() const noexcept;
	const_iterator cend() const noexcept;

	Eigen::Map<matrix_type> as_matrix();

	img& srgb2lrgb();
	img& lrgb2oklab();

private:
	//======//
	// DATA //
	//======//

	std::unique_ptr<array_type> m_data;
};

//=========//
// ALIASES //
//=========//

template <uint32_t N, uint32_t M>
using u8 = img<uint8_t,N,M>;

template <uint32_t N, uint32_t M>
using f64 = img<double,N,M>;

//=============//
// NON-MEMBERS //
//=============//

void write_header(const uint32_t, const uint32_t);
std::pair<uint32_t,uint32_t> read_header();

template <uint32_t N, uint32_t M>
void write_img(const img<uint8_t,N,M>&);

template <uint32_t N, uint32_t M>
f64<N,M> read_img();

//===================================//

//=======//
// CTORS //
//=======//

template <typename T, uint32_t N, uint32_t M>
img<T,N,M>::img() :
	m_data(new array_type())
{}

//================//
// PUBLIC MEMBERS //
//================//

template <typename T, uint32_t N, uint32_t M>
typename img<T,N,M>::colour_type&
img<T,N,M>::operator()(const uint32_t x, const uint32_t y)
	{return (*m_data)[N*y + x];}
template <typename T, uint32_t N, uint32_t M>
const typename img<T,N,M>::colour_type&
img<T,N,M>::operator()(const uint32_t x, const uint32_t y) const
	{return (*m_data)[N*y + x];}

template <typename T, uint32_t N, uint32_t M>
typename img<T,N,M>::iterator
img<T,N,M>::begin() noexcept
	{return m_data->begin();}
template <typename T, uint32_t N, uint32_t M>
typename img<T,N,M>::const_iterator
img<T,N,M>::begin() const noexcept
	{return cbegin();}
template <typename T, uint32_t N, uint32_t M>
typename img<T,N,M>::const_iterator
img<T,N,M>::cbegin() const noexcept
	{return m_data->cbegin();}

template <typename T, uint32_t N, uint32_t M>
typename img<T,N,M>::iterator
img<T,N,M>::end() noexcept
	{return m_data->end();}
template <typename T, uint32_t N, uint32_t M>
typename img<T,N,M>::const_iterator
img<T,N,M>::end() const noexcept
	{return cend();}
template <typename T, uint32_t N, uint32_t M>
typename img<T,N,M>::const_iterator
img<T,N,M>::cend() const noexcept
	{return m_data->cend();}

template <typename T, uint32_t N, uint32_t M>
Eigen::Map<typename img<T,N,M>::matrix_type>
img<T,N,M>::as_matrix()
	{return Eigen::Map<matrix_type>(&((*m_data)[0][0]), 3,N*M);}

template <typename F, uint32_t N, uint32_t M>
img<F,N,M>&
img<F,N,M>::srgb2lrgb()
{
	for (colour_type& i : *m_data)
		i.srgb2lrgb();
	return *this;
}

template <typename F, uint32_t N, uint32_t M>
img<F,N,M>&
img<F,N,M>::lrgb2oklab()
{
	Eigen::Map<matrix_type> X(as_matrix());
	X = oklab::M2*((oklab::M1*X).array().pow(1.0/3.0).matrix());
	return *this;
}

//=============//
// NON-MEMBERS //
//=============//

template <uint32_t N, uint32_t M>
void
write_img(const img<uint8_t,N,M>& img)
{
	write_header(N,M);
	for (const auto& i : img)
		colour::write_pixel(i);
}

template <uint32_t N, uint32_t M>
f64<N,M>
read_img()
{
	if (read_header() != std::pair<uint32_t,uint32_t>(N,M))
		die("Dimensions don't match");

	f64<N,M> ret;
	for (typename f64<N,M>::colour_type& i : ret)
		i = colour::read_pixel().as_f64();
	return ret;
}

} // ns img
