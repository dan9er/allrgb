# flags
CPPFLAGS = -D_DEFAULT_SOURCE
CXXFLAGS = -std=c++11 -pedantic -Wall -Wextra
LDFLAGS  = -s

# release flags
CXXFLAGS += -O3 -march=native -mtune=native -flto
LDFLAGS  += -flto

# debug flags
#CXXFLAGS += -O0 -g3
#LDFLAGS  += -g3

# compiler and linker
CXX = clang++
